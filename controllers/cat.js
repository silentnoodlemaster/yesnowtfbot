'use strict';

const Telegram = require('telegram-node-bot');
const request = require('request');

var url = 'http://thecatapi.com/api/images/get?format=src&type=jpg,png';

class CatController extends Telegram.TelegramBaseController {

	catHandler($) {
		request.get({
			url: url,
			headers: { 'User-Agent': 'request' }
		}, (err, res) => {
			if (err) {
				console.error('Error:', err);
			} else if (res.statusCode !== 200) {
				console.warn('Status:', res.statusCode);
			} else {
				$.sendPhoto({ url: '', filename: 'image.jpg'});
			}
		});

	}
	get routes() {
		return {
			'catCommand': 'catHandler'
		};
	}
}
module.exports = CatController;