'use strict';

const Telegram = require('telegram-node-bot');
var request = require('request');
var url = 'https://yesno.wtf/api';
const CallbackQueryController = require('../controllers/callback');
class InlineModeController extends Telegram.TelegramBaseInlineQueryController {

	handle($) {

		request.get({
			url: url,
			json: true,
			headers: { 'User-Agent': 'request' }
		}, (err, res, data) => {
			if (err) {
				console.error('Error:', err);
			} else if (res.statusCode !== 200) {
				console.warn('Status:', res.statusCode);
			} else {
				//$.sendDocument({ url: data.image, filename: data.answer + '.gif'});
				$.update.callbackQuery = new CallbackQueryController;
			}
		});
	}
	//get routes() {
	//	return {
	//		'callbackQuery':''
	//	};
	//}
}
module.exports = InlineModeController;