'use strict';

const Telegram = require('telegram-node-bot');
const request = require('request');

var url = 'http://inspirobot.me/api?generate=true';

class InspireController extends Telegram.TelegramBaseController {

	inspireHandler($) {
		request.get({
			url: url,
			headers: { 'User-Agent': 'request' }
		}, (err, res, data) => {
			if (err) {
				console.error('Error:', err);
			} else if (res.statusCode !== 200) {
				console.warn('Status:', res.statusCode);
			} else {
				$.sendPhoto({ url: data, filename: 'image.jpg'});
			}
		});

	}
	get routes() {
		return {
			'inspireCommand': 'inspireHandler'
		};
	}
}
module.exports = InspireController;