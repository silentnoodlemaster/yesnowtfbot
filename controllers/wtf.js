'use strict';

const Telegram = require('telegram-node-bot');
const request = require('request');

var url = 'https://yesno.wtf/api';

class WtfController extends Telegram.TelegramBaseController {

	wtfHandler($) {
		request.get({
			url: url,
			json: true,
			headers: { 'User-Agent': 'request' }
		}, (err, res, data) => {
			if (err) {
				console.error('Error:', err);
			} else if (res.statusCode !== 200) {
				console.warn('Status:', res.statusCode);
			} else {
				$.sendDocument({ url: data.image, filename: data.answer + '.gif' });
			}
		});

	}
	get routes() {
		return {
			'wtfCommand': 'wtfHandler'
		};
	}
}
module.exports = WtfController;