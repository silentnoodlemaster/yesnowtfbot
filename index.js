'use strict';

const Telegram = require('telegram-node-bot'),
	tg = new Telegram.Telegram('API key here', {
		workers: 1,
		webAdmin: {
			port: 7778,
			host: 'localhost'
		}

	});

const StartController = require('./controllers/start'),
	WtfController = require('./controllers/wtf'),
	InspireController = require('./controllers/inspire'),
	XmasController = require('./controllers/xmas');

tg.router.when(new Telegram.TextCommand('/start', 'startCommand'), new StartController())
	.when(new Telegram.TextCommand('/wtf', 'wtfCommand'), new WtfController())
	.when(new Telegram.TextCommand('/inspire', 'inspireCommand'), new InspireController())
	.when(new Telegram.TextCommand('/xmas', 'xmasCommand'), new XmasController());